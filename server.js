// const express = require("express");
// const bodyParser = require("body-parser");
// const weatherApi = require("./utility/weather");
// const pokemon = require("./utility/pokemon");
// const puppeteer = require("puppeteer");
// const app = express();
// app.use(bodyParser.json());

// app.post("/webhookDialog", async (req, res) => {
//   const auth = req.headers["authorization"];
//   if (!auth) {
//     res
//       .status(401)
//       .setHeader("WWW-Authenticate", 'Basic realm="Secure Area"')
//       .send("no auth!")
//       .end();
//   } else if (auth) {
//     const plain_auth = new Buffer(auth.split(" ")[1], "base64").toString();
//     const [username, password] = plain_auth.split(":");
//     if (username == "admin" && password == "ns333") {
//       const event = req.body.originalDetectIntentRequest.payload.data;
//       const text = event.message.text.toLowerCase();
//       const body = req.body;

//       if (body.queryResult.action == `askingtheweather.askingtheweather-custom`) {
//         const result = await handleWeather({
//           city: {
//             台中: "Taichung",
//             台北: "Taipei",
//             台灣: "Taiwan",
//             高雄: "Kaohsiung"
//           }[text]
//         });
//         return res.json(result);
//       }

//       if (body.queryResult.action == `searchpokemon.searchpokemon-custom`) {
//         const result = await handlePokemon({ name: text });
//         return res.json(result);
//       }

//       if (body.queryResult.action == `ask.church.info`) {
//         const result = await getSermon();
//         return res.json(result);
//       }
//     }
//   }
// });

// app.listen(process.env.PORT || 8080);

// async function getSermon() {
//   const browser = await puppeteer.launch(
//     { args: ['--no-sandbox', '--disable-setuid-sandbox'] }
//     );
//   const page = await browser.newPage();
//   await page.goto("https://www.bannerch.org/media-zone/sunday-information");
//   await page.evaluate(() => {
//     const week = Math.floor(new Date().getDate() / 7) - 1;
//     document
//       .querySelectorAll(".smaller-cont")
//       [week].querySelector("a")
//       .click();
//   });
//   // await delay(2000);
//   const sermonUrl = await page.evaluate(() => {
//     return {
//       fulfillmentText: document.querySelector(".sermons-media iframe").src
//     };
//   });
//   console.log(sermonUrl);
//   await browser.close();
//   return sermonUrl;
// }

// const handleWeather = async ({ city }) => {
//   try {
//     const output = await weatherApi(city, "");
//     return { fulfillmentText: output };
//   } catch (error) {
//     return {
//       fulfillmentText: `I don't know the weather but I hope it's good!`
//     };
//   }
// };

// const handlePokemon = async ({ name }) => {
//   const pImageUrl = pokemon.getSprite(name);
//   if (pImageUrl) {
//     return {
//       fulfillmentMessages: [
//         {
//           image: {
//             imageUri: pImageUrl,
//             accessibilityText: name
//           }
//         }
//       ]
//     };
//   } else {
//     return {
//       fulfillmentText: `I don't know the weather but I hope it's good!`
//     };
//   }
// };
// // const delay = time => new Promise(resolve => setTimeout(() => resolve(), time));

// // async function getSermon() {
// //   const nightmare = Nightmare();
  
// //   return nightmare
// //     .goto('https://www.bannerch.org/media-zone/sunday-information')
// //     .evaluate(() => {
// //       const week = Math.floor(new Date().getDate() / 7) - 1;
// //       document
// //         .querySelectorAll(".smaller-cont")
// //         [week].querySelector("a")
// //         .click();
// //       return ({fulfillmentText: document.querySelector(".sermons-media iframe").src})
// //     })
// //     .end()
// //     .then((sermonUrl) => sermonUrl);
// // }



const keystone = require('keystone');

keystone.init({
	'name': 'SYME English Cafe',
	'brand': 'SYME',
	'auto update': true,
	'session': true,
	'auth': true,
  'user model': 'User',
  'cookie secret': 'ns3',
  'port': process.env.PORT || 8080,
  'mongo': process.env.MONGO_URI,
});

keystone.set('routes', require('./routes'));

keystone.import('models');

keystone.start();

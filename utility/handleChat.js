const i18n = require("i18n");
const { setUserStatus, getUnitOutline, getUnitInfo, getUnitQuiz, client, encodeStatus, decodeStatus, getStatus } = require('./index');
const optionsTable = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];
const callWeatherApi = require('./weather');
const pokemon = require('./pokemon');
const { SYME_INIT_MENU, SYME_QUIZ_QUICK_REPLY, SYME_QUIZ_ANSWER } = require('../utility/lineTemplate');

const hanldeChat = ({event, status}) => {
  const handleWeather = async ({ city }) => {
    try {
      const output = await callWeatherApi(city, '');
      return ({ fulfillmentText: output });
    } catch (error) {
      return ({ fulfillmentText: `I don't know the weather but I hope it's good!` });
    }
  }
  const handlePokemon = async ({ name }) => {
    const pImageUrl = pokemon.getSprite(name);
    if (pImageUrl) {
      return ({ fulfillmentMessages: [{
        image: {
          "imageUri": pImageUrl,
          "accessibilityText": name
        }
      }]})
      // return ({ fulfillmentText: pImageUrl });
    } else {
      return ({ fulfillmentText: `I don't know the weather but I hope it's good!` });
    }
  }

  const handleInitStatus = async () => {
    const text = event.message.text;
    const userId = event.source.userId;
    let responseText = i18n.__('statusError');
    try {
      if (text === '讀書' || text == '學習') {
        return SYME_INIT_MENU;
      }
      if (text === '1') {
        responseText = await getUnitOutline();
      }
      if (text === '2') {
        setUserStatus(userId, 'studyUnit$0');
        responseText = i18n.__('chooseUnit');
      }
      if (text === '3') {
        const status = {
          current: 0,
          wrongTime: 0,
          quizzes: [],
          next: false,
          chooseUnit: true,
          showQuiz: true,
          correct: 0,
        }
        setUserStatus(userId, `testUnit${encodeStatus(status)}`);
        responseText = i18n.__('chooseUnit');
      }
      return ({
        fulfillmentText: responseText,
      });
    } catch (error) {
      console.log(error);
      return ({
        fulfillmentText: responseText,
      });
    }
  }
  
  const handleStudyUnit = async () => {
    const text = event.message.text;
    const userId = event.source.userId;
    const unitInfo = await getUnitInfo(Number(text));
  
    if (unitInfo) {
      setUserStatus(userId, '');
      return ({
        fulfillmentText: `${unitInfo.en_title}\n\n${unitInfo.en_desc}`,
      });
    } else {
      const retryFlag = Number(status.match(/studyUnit?(\$.*)?|(\#.*)/)[1].slice(1));
      if (retryFlag < 3) {
        setUserStatus(userId, `studyUnit$${retryFlag + 1}`);
        return ({
          fulfillmentText: i18n.__('errorRetryFindUnit').replace('flag', retryFlag + 1),
        });
      } else {
        return handleExit();
      }
    }
  }
  
  const handleExit = async () => {
    const userId = event.source.userId;
    setUserStatus(userId, '');
    return SYME_INIT_MENU;
  }
  
  const handleTestUnit = async ({ status: _status }) => {
    const text = event.message.text.toLowerCase();
    const userId = event.source.userId;
    const { wrongTime, quizzes, next, chooseUnit, current, showQuiz, correct } = _status;
  
    try {
      if (chooseUnit) {
        const initQuizzes = await getUnitQuiz(text, { A: 4, B: 3, C: 3 }); // text是unit
        if (!initQuizzes) throw Error('no the unit-chooseUnit');
  
        const options = initQuizzes[0].options.reduce((acc, val, i) => `${acc}\n(${optionsTable[i]}) : ${val}`,'');
  
        setUserStatus(userId, `testUnit${encodeStatus({..._status, current: current + 1, quizzes: initQuizzes, next: true, chooseUnit: false, showQuiz: false, wrongTime: 0 })}`);
        return SYME_QUIZ_QUICK_REPLY({
          quizText: `第${current + 1}/${initQuizzes.length}題\n${initQuizzes[0].question.replace('_', '______')}\n${options}`,
          optionsNumber: initQuizzes[0].options.length
        });
      }
  
      if (showQuiz) {
        const options = quizzes[current].options.reduce((acc, val, i) => `${acc}\n(${optionsTable[i]}) : ${val}`,'');
        setUserStatus(userId, `testUnit${encodeStatus({..._status, current: current + 1, showQuiz: false, wrongTime: 0})}`);
        return SYME_QUIZ_QUICK_REPLY({
          quizText: `第${current + 1}/${quizzes.length}題\n${quizzes[current].question.replace('_', '______')}\n${options}`,
          optionsNumber: quizzes[current].options.length
        });
      }
  
      if (current <= quizzes.length) {
        const quiz = quizzes[current - 1];
        const quizAns = quiz.options[quiz.answer - 1];
        const isCorrect = optionsTable[quiz.answer - 1].toLowerCase() === text;
        const checkCorrectText = (isCorrect) ? '恭喜你答對了！' : '答錯囉！';
        const correctFlag = (isCorrect) ? correct + 1 : correct;
        const nextText = (current == quizzes.length) ? '恭喜你完成測驗' : '前往下一題請輸入next';
        const newStatus = (current == quizzes.length) ? '' : `testUnit${encodeStatus({..._status, correct: correctFlag, next: current != quizzes.length, showQuiz: true, wrongTime: 0 })}`;
        const correctText = (current == quizzes.length) ? `你的正確率為${correctFlag / quizzes.length * 100}%(${correctFlag}/${quizzes.length})` : '';
        if (optionsTable.slice(0, quiz.options.length).map(i => i.toLowerCase()).indexOf(text) == -1) 
          throw Error('the type is wrong - answer quiz');
        setUserStatus(userId, newStatus);
        return SYME_QUIZ_ANSWER({
          quizText: `第${current}/${quizzes.length}題-${checkCorrectText}\n正確答案為: (${optionsTable[quiz.answer - 1]}). ${quizAns}\n\n${quiz.question.replace('_', `${quizAns}`)}\n\n${nextText}${correctText}`,
          quizNextText: (current == quizzes.length) ? '完成' : '下一題'
        })
      }
    } catch (error) {
      console.log(error);
      if (wrongTime < 3) {
        setUserStatus(userId, `testUnit${encodeStatus({..._status, wrongTime: wrongTime + 1})}`);
        return ({
          fulfillmentText: i18n.__('errorRetryFindUnit').replace('flag', wrongTime + 1),
        });
      } else {
        return handleExit();
      }
    }
  }
  return {
    handleInitStatus,
    handleStudyUnit,
    handleExit,
    handleTestUnit,
    handleWeather,
    handlePokemon
  }
}


module.exports = hanldeChat;
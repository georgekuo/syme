const keystone = require("keystone");
const line = require("@line/bot-sdk");
const i18n = require("i18n");
const R = require('ramda');

const config = {
  channelId: process.env.CHANNELID,
  channelAccessToken: process.env.CHANNELACCESSTOKEN,
  channelSecret: process.env.CHANNELSECERT
};

const client = new line.Client(config);

/**
 * Object -> String
 * @param {Object} status 
 */
const encodeStatus = (status) => encodeURIComponent(JSON.stringify(status));

/**
 * (status, job) -> Object
 * @param {String} status  
 * @param {String} job testUnit, studyUnit
 */
const decodeStatus = R.pipe(
  (status, job) => status.match(new RegExp(`${job}(.*)`))[1],
  (status) => JSON.parse(decodeURIComponent(status)) // decodeStatus
)


const setUserStatus = (userId, status) => {
  const UserInfo = keystone.list("UserInfo").model;
  UserInfo.update({ userId }, { $set: { status } }, { multi: true },
    e => {
      if (e) console.log(e);
    }
  );
}

const getUnitOutline = () => new Promise((resolve, reject) => {
  const Unit = keystone.list("Unit").model;
  Unit.find({}, (err, data) => {
    return resolve(data.reduce((acc, val) =>  `${acc}${val.en_title}\n${val.zh_TW_title}\n\n`, ''));
  });
});

const getStatus = (userId) => new Promise((resolve, reject) => {
  const UserInfo = keystone.list("UserInfo").model;
  UserInfo.findOne({ userId }, (err, data) => {
    if (data) {
      i18n.setLocale(data.language);
      return resolve(data.status);
    } else {
      i18n.setLocale('zh_TW');
      client.getProfile(userId).then((profile) => {
        const userInfo = new UserInfo({
          userId,
          status: '',
          quizs: [],
          name: profile.displayName,
          language: 'zh_TW',
          avatar: profile.pictureUrl,
          timestamp: new Date().getTime(),
        });
        userInfo.save(err => {
          if (err) return reject(err);
          return resolve('');
        });
      }).catch(err => console.log(err));
    }
  });
});

const getUnitInfo = (unit) => new Promise((resolve, reject) => {
  const Unit = keystone.list("Unit").model;
  Unit.findOne({ unit }, (err, data) => resolve(data));
});

const shuffle = (a,b) => Math.random() > 0.5 ? -1:1;

/**
 * 
 * @param {String} unit 
 * @param {Object} {A, B ,C} for how many question of section it should take
 */
const getUnitQuiz = (unit, { A, B, C }) => new Promise((resolve, reject) => {
  const Quiz = keystone.list("Quiz").model;
  Quiz.find({ unit }, (err, data) => {
    if (!data) resolve(null);
    const sectionA = data.filter((i) => i.section === 'A').sort(shuffle).slice(0, A);
    const sectionB = data.filter((i) => i.section === 'B').sort(shuffle).slice(0, B);
    const sectionC = data.filter((i) => i.section === 'C').sort(shuffle).slice(0, C);
    resolve([...sectionA, ...sectionB, ...sectionC]);
  });
});

module.exports = {
  setUserStatus,
  getUnitOutline,
  getStatus,
  getUnitInfo,
  getUnitQuiz,
  client,
  encodeStatus,
  decodeStatus
}

const optionsTable = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"];
const SYME_INIT_MENU = {
  fulfillmentMessages: [
    {
      payload: {
        line: {
          type: "template",
          altText: "this is a buttons template",
          template: {
            type: "buttons",
            actions: [
              {
                type: "postback",
                label: "課程大綱",
                text: "1",
                data: "1"
              },
              {
                type: "postback",
                label: "閱讀單元",
                text: "2",
                data: "2"
              },
              {
                type: "postback",
                label: "模擬試題",
                text: "3",
                data: "3"
              }
            ],
            title: "歡迎來到SYME英文學習天地",
            text: "菜單"
          }
        }
      }
    }
  ]
};
const SYME_QUIZ_QUICK_REPLY = ({ quizText, optionsNumber }) => {
  return {
    fulfillmentMessages: [
      {
        payload: {
          line: {
            type: "text",
            altText: "this is a quiz quick reply",
            text: quizText,
            quickReply: {
              items: new Array(optionsNumber).fill(1).map((i, index) => ({
                type: "action",
                action: {
                  type: "message",
                  label: optionsTable[index],
                  text: optionsTable[index]
                }
              }))
            }
          }
        }
      }
    ]
  };
};
// const SYME_QUIZ_ANSWER = ({ quizText, quizNextText }) => {
//   return {
// fulfillmentMessages: [
//   {
//     quickReplies: {
//       "title": 'Hello',
//       "quickReplies": [
//         'a','b','c','d'
//       ]
//     },
//   },
// ]
//   };
// };
const SYME_QUIZ_ANSWER = ({ quizText, quizNextText }) => {
  return {
    fulfillmentMessages: [
      {
        payload: {
          line: {
            type: "text",
            altText: "this is a quiz quick reply",
            text: quizText,
            quickReply: {
              items: [
                {
                  type: "action",
                  action: {
                    type: "message",
                    label: quizNextText,
                    text: quizNextText == "完成" ? "學習" : "next"
                  }
                }
                // {
                //   type: "action",
                //   action: {
                //     type: "datetimepicker",
                //     label: "Select date",
                //     data: "storeId=12345",
                //     mode: "datetime"
                //   }
                // },
                // {
                //   type: "action",
                //   action: {
                //     type: "postback",
                //     label: "Buy",
                //     data: "action=buy&itemid=123"
                //   }
                // }
                // {
                //   type: "action",
                //   action: {
                //     type: "location",
                //     label: "Send location"
                //   }
                // },
                // {
                //   type: "action",
                //   action: {
                //     type: "camera",
                //     label: "Camera"
                //   }
                // },
                // {
                //   type: "action",
                //   action: {
                //     type: "cameraRoll",
                //     label: "Camera roll"
                //   }
                // }
              ]
            }
          }
        }
      }
    ]
  };
};

module.exports = {
  SYME_INIT_MENU,
  SYME_QUIZ_QUICK_REPLY,
  SYME_QUIZ_ANSWER
};

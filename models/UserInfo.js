const keystone = require('keystone');
const Types = keystone.Field.Types;

/**
 * UserInfo Model
 * ==========
 */
const UserInfo = new keystone.List('UserInfo');

UserInfo.add({
	userId:   { type: Types.Text, initial: true, required: true, index: true, unique: true },
  status:   { type: Types.Text, initial: true, noedit: true },
  language: { type: Types.Text, initial: true, required: true },
  name:     { type: Types.Name, initial: true },
  avatar:   { type: Types.Url,  initial: true },
  timestamp:  { type: Types.Date, initial: true }
});

/** 
 * Registration
 */
UserInfo.defaultColumns = 'userId status name language avatar timestamp';
UserInfo.register();

const keystone = require('keystone');
const Types = keystone.Field.Types;

/**
 * Quiz Model
 * ==========
 */
const Quiz = new keystone.List('Quiz');

Quiz.add({
	quizId: { type: Types.Text, initial: true, required: true, index: true },
  level: { type: Types.Number, initial: true, required: true},
  unit: { type: Types.Number, initial: true, required: true},
  section: { type: Types.Text, initial: true, required: true},
  question: { type: Types.Textarea, initial: true, required: true},
  answer: { type: Types.Number, initial: true, required: true },
  options: {type: Types.TextArray, initial: true, required: true },
});

/**
 * Registration
 */
Quiz.defaultColumns = 'quizId level unit section question answer options unitDesc';
Quiz.register();

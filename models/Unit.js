const keystone = require('keystone');
const Types = keystone.Field.Types;

/**
 * Quiz Model
 * ==========
 */
const Unit = new keystone.List('Unit');

Unit.add({
	unit: { type: Types.Number, initial: true, required: true, index: true, unique: true },
  en_title: { type: Types.Text, initial: true, required: true },
  en_desc: { type: Types.Textarea, initial: true, required: true },
  zh_TW_title: { type: Types.Text, initial: true, required: true },
  zh_TW_desc: { type: Types.Textarea, initial: true, required: true },
});

/**
 * Registration
 */
Unit.defaultColumns = 'unit title en_desc zh_TW_desc';
Unit.register();

const { routeApi } = require('../api/index');

// Setup Route Bindings
exports = module.exports = function (app) {
  routeApi(app);
};

const handleChat = require('../utility/handleChat');
const { getStatus, decodeStatus } = require('../utility/index');
const CITY = require('../utility/city');
const i18n = require("i18n");
// const {SYME_INIT_MENU} = require('../utility/lineTemplate.js')
i18n.configure({
  locales:['en', 'zh_TW'],
  directory: './_locales',
  defaultLocale: 'zh_TW',
});
// return handleExit({event}, '神曲支援:https://www.youtube.com/watch?v=T0LfHEwEXXw&feature=youtu.be&t=1m13s');

const execCommand = (req, res) => async ({event, status, body}) => {
  const text = event.message.text.toLowerCase();
  const { handleInitStatus, handleStudyUnit, handleExit, handleTestUnit, handleWeather, handlePokemon } = handleChat({event, status});

  if (text == 'exit' || text == '離開') {
    const result = await handleExit();
    return res.json(result);
  }

  if (body.queryResult.action == `askingtheweather.askingtheweather-custom`) {
    const result = await handleWeather({city: CITY[text] });
    return res.json(result);
  }

  if (body.queryResult.action == `searchpokemon.searchpokemon-custom`) {
    const result = await handlePokemon({name: text });
    return res.json(result);
  }

  if (status == '') {
    const result = await handleInitStatus();
    return res.json(result);
  }

  if (status.startsWith('studyUnit')) {
    const result = await handleStudyUnit();
    return res.json(result);
  }

  if (status.startsWith('testUnit')) {
    const result = await handleTestUnit({status: decodeStatus(status, 'testUnit')});
    return res.json(result);
  }
}

const handleEvent = (req, res) => async ({ event, body }) => {
  if (event.replyToken == '00000000000000000000000000000000') return;
  if (event.type !== "message" || event.message.type !== "text") return;

  const status = await getStatus(event.source.userId);

  execCommand(req, res)({event, status, body});
};

function routeApi(app) {
  const keystone = require("keystone");
  const Quiz = keystone.list("Quiz").model;
  const Unit = keystone.list("Unit").model;
  const UserInfo = keystone.list("UserInfo").model;

  app.get("/", (req, res) => {
    res.redirect("/keystone/signin");
  });

  app.get("/quiz", (req, res) => {
    Quiz.find({}, (err, data) => {
      res.status(200).send(data);
    });
  });

  app.get("/quiz/:unit", (req, res) => {
    Quiz.findOne({ unit: req.params.unit }, (err, data) => {
      res.status(200).send(data);
    });
  });

  app.get("/unit/:unit", (req, res) => {
    Unit.findOne({ unit: req.params.unit }, (err, data) => {
      res.status(200).send(data);
    });
  });

  app.get("/unit", (req, res) => {
    Unit.find({}, (err, data) => {
      res.status(200).send(data);
    });
  });

  app.get("/user/:userId", (req, res) => {
    UserInfo.findOne({ userId: req.params.userId }, (err, data) => {
      res.status(200).send(data);
    });
  });

  app.post("/user", (req, res) => {
    UserInfo.update({ userId: req.body.userId }, { $set: { status: req.body.status } }, { multi: true },
      e => {
        if (e) {
          console.log(e);
          res.end();
        } else {
          res.status(200).send(`更新User Info成功`).end();
        }
      }
    );
  });

  app.post("/webhook", (req, res) => {
    const signature = req.headers["x-line-signature"];
    if (!signature) {
      return res
        .status(403)
        .send("no x-line-signature")
        .end();
    }
    
    Promise.all(req.body.events.map((event) => handleEvent({event, body: req.body })))
      .then(result => res.json(result))
      .catch(err => console.log(err));
  });
  app.post("/webhookDialog", (req, res) => {
    const auth = req.headers['authorization'];
    if (!auth) {
      res.status(401)
      .setHeader('WWW-Authenticate', 'Basic realm="Secure Area"')
      .send('no auth!')
      .end();
    } else if (auth) {
      const plain_auth = new Buffer(auth.split(' ')[1], 'base64').toString();
      const [username, password] = plain_auth.split(':'); 
      if (username == 'admin' && password == 'ns333') {
        // res.json(SYME_INIT_MENU)
        const event = req.body.originalDetectIntentRequest.payload.data;
        handleEvent(req, res)({event, body: req.body});
      }
    }
  });
}

exports.routeApi = routeApi;
